package ru.kondratenko.jse49.data;

public interface FormDataSender {
    /**
     * Отправляет данные формы.
     * @param data данные формы.
     */
    void send(String data);

}
