package ru.kondratenko.jse49.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ConsoleFormDataSender implements FormDataSender{
    @Override
    public void send(String data) {
        System.out.println("ConsoleFormDataSender works.");
    }
}
