package ru.kondratenko.jse49;

import ru.kondratenko.jse49.command.CommandInterface;
import ru.kondratenko.jse49.data.FormDataSender;
import ru.kondratenko.jse49.visa.IndiaVisaForm;
import ru.kondratenko.jse49.visa.USAVisaForm;

public class ApplicationProcess {
    public static final String EXIT = "exit";
    private CommandInterface commandInterface;
    private FormDataSender formDataSender;

    public ApplicationProcess(CommandInterface commandInterface, FormDataSender formDataSender) {
        this.commandInterface = commandInterface;
        this.formDataSender = formDataSender;
    }

    public void progress() throws UnsupportedOperationException {
        String command = "";
        String data = "";
        while (!EXIT.equals(command)) {
            command = commandInterface.writeAndRead("Choose country: India or USA?");
            switch (command) {
                case "India":
                    IndiaVisaForm indiaVisaForm = new IndiaVisaForm();
                    indiaVisaForm.fillForm(commandInterface);
                    data = indiaVisaForm.getFormData();
                    break;
                case "USA":
                    USAVisaForm usaVisaForm = new USAVisaForm();
                    usaVisaForm.fillForm(commandInterface);
                    data = usaVisaForm.getFormData();
                    break;
                default:
                    commandInterface.write("Unknown country.");
            }
            if (!data.isEmpty()) {
                formDataSender.send(data);
                data = "";
            }
        }

    }

}

