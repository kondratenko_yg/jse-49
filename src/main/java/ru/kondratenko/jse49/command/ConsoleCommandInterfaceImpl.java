package ru.kondratenko.jse49.command;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@Primary
public class ConsoleCommandInterfaceImpl implements ConsoleCommandInterface {
    Scanner scanner = new Scanner(System.in);

    @Override
    public String writeAndRead(String text) {
        System.out.println(text);
        return scanner.nextLine();
    }

    @Override
    public void write(String text) {
        System.out.println(text);
    }

}
