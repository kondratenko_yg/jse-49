package ru.kondratenko.jse49.visa;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kondratenko.jse49.command.CommandInterface;

@Component
@Scope("prototype")
public class USAVisaForm implements VisaApplicationForm{
    private String name;
    private String lastName;
    private Integer age;

    @Override
    public void fillForm(CommandInterface commandInterface) {
        this.name = commandInterface.writeAndRead("Name: ");
        this.lastName = commandInterface.writeAndRead("LastName: ");
        this.age = Integer.parseInt(commandInterface.writeAndRead("Age: "));
    }

    @Override
    public String getFormData() {
        return "USAVisaForm: "+this.name + ", " + this.lastName + ", " + this.age.toString();
    }
}
