package ru.kondratenko.jse49.visa;

import ru.kondratenko.jse49.command.CommandInterface;

public interface VisaApplicationForm {
    /**
     * Запрашивает и заполняет данные формы.
     */
    void fillForm(CommandInterface commandInterface);

    /**
     * Возвращает данные формы.
     */
    String getFormData();

}
