package ru.kondratenko.jse49.visa;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kondratenko.jse49.command.CommandInterface;

@Component
@Scope("prototype")
public class IndiaVisaForm implements VisaApplicationForm{
    private String name;
    private String lastName;

    @Override
    public void fillForm(CommandInterface commandInterface) {
        this.name = commandInterface.writeAndRead("Name: ");
        this.lastName = commandInterface.writeAndRead("LastName: ");
    }

    @Override
    public String getFormData() {
        return "IndiaVisaForm: "+this.name + ", " + this.lastName;
    }

}
