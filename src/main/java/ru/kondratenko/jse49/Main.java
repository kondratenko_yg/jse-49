package ru.kondratenko.jse49;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kondratenko.jse49.command.CommandInterface;
import ru.kondratenko.jse49.data.ConsoleFormDataSender;
import ru.kondratenko.jse49.data.FormDataSender;

import java.io.IOException;
@Log4j2
public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(AppContext.class);

        CommandInterface commandInterface =(CommandInterface) ctx.getBean("consoleCommandInterfaceImpl");
        //CommandInterface commandInterface =(CommandInterface) ctx.getBean("voiceCommandInterfaceImpl");
        FormDataSender formDataSender = (FormDataSender) ctx.getBean("consoleFormDataSender");
        //FormDataSender formDataSender = (FormDataSender) ctx.getBean("fileFormDataSender");
        ApplicationProcess applicationProcess = new ApplicationProcess(commandInterface,formDataSender);
        try{
            applicationProcess.progress();
        }catch(RuntimeException e){
            log.error(e.getClass() + (e.getMessage() == null?"":e.getMessage()));
        }
    }
}
